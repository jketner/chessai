use alpha_beta_rust::board::Board;


#[test]
fn perft() {
    let mut board = Board::new();
    board.initialize("startpos").unwrap();
    assert_eq!(board.perft_node(0), 1, "perft 0");
    assert_eq!(board.perft_node(1), 20, "perft 1");
    assert_eq!(board.perft_node(2), 400, "perft 2");
    assert_eq!(board.perft_node(3), 8902, "perft 3");
    assert_eq!(board.perft_node(4), 197281, "perft 4");
    assert_eq!(board.perft_node(5), 4865609, "perft 5");
    assert_eq!(board.perft_node(6), 119060324, "perft 6");
}

#[test]
fn kiwipete() {
    let mut board = Board::new();
    board.initialize("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 0").unwrap();
    assert_eq!(board.perft_node(0), 1, "kiwipete 0");
    assert_eq!(board.perft_node(1), 48, "kiwipete 1");
    assert_eq!(board.perft_node(2), 2039, "kiwipete 2");
    assert_eq!(board.perft_node(3), 97862, "kiwipete 3");
    assert_eq!(board.perft_node(4), 4085603, "kiwipete 4");
    assert_eq!(board.perft_node(5), 193690690, "kiwipete 5");
}

#[test]
fn position_3() {
    let mut board = Board::new();
    board.initialize("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 0").unwrap();
    assert_eq!(board.perft_node(0), 1, "position_3 0");
    assert_eq!(board.perft_node(1), 14, "position_3 1");
    assert_eq!(board.perft_node(2), 191, "position_3 2");
    assert_eq!(board.perft_node(3), 2812, "position_3 3");
    assert_eq!(board.perft_node(4), 43238, "position_3 4");
    assert_eq!(board.perft_node(5), 674624, "position_3 5");
    assert_eq!(board.perft_node(6), 11030083, "position_3 5");
}

#[test]
fn position_4() {
    let mut board = Board::new();
    board.initialize("r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1").unwrap();
    assert_eq!(board.perft_node(0), 1, "position_4 0");
    assert_eq!(board.perft_node(1), 6, "position_4 1");
    assert_eq!(board.perft_node(2), 264, "position_4 2");
    assert_eq!(board.perft_node(3), 9467, "position_4 3");
    assert_eq!(board.perft_node(4), 422333, "position_4 4");
    assert_eq!(board.perft_node(5), 15833292, "position_4 5");
}

#[test]
fn position_4_mirror() {
    let mut board = Board::new();
    board.initialize("r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1").unwrap();
    assert_eq!(board.perft_node(0), 1, "position_4_mirror 0");
    assert_eq!(board.perft_node(1), 6, "position_4_mirror 1");
    assert_eq!(board.perft_node(2), 264, "position_4_mirror 2");
    assert_eq!(board.perft_node(3), 9467, "position_4_mirror 3");
    assert_eq!(board.perft_node(4), 422333, "position_4_mirror 4");
    assert_eq!(board.perft_node(5), 15833292, "position_4_mirror 5");
}

#[test]
fn position_5() {
    let mut board = Board::new();
    board.initialize("rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8").unwrap();
    assert_eq!(board.perft_node(0), 1, "position_5 0");
    assert_eq!(board.perft_node(1), 44, "position_5 1");
    assert_eq!(board.perft_node(2), 1486, "position_5 2");
    assert_eq!(board.perft_node(3), 62379, "position_5 3");
    assert_eq!(board.perft_node(4), 2103487, "position_5 4");
    assert_eq!(board.perft_node(5), 89941194, "position_5 5");
}

#[test]
fn position_6() {
    let mut board = Board::new();
    board.initialize("r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10").unwrap();
    assert_eq!(board.perft_node(0), 1, "position_5 0");
    assert_eq!(board.perft_node(1), 46, "position_5 1");
    assert_eq!(board.perft_node(2), 2079, "position_5 2");
    assert_eq!(board.perft_node(3), 89890, "position_5 3");
    assert_eq!(board.perft_node(4), 3894594, "position_5 4");
    assert_eq!(board.perft_node(5), 164075551, "position_5 5");
}
