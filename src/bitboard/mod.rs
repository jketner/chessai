#![warn(clippy::all)]
use std::ops;
extern crate bitintr;
use bitintr::*;

pub const FIRST_RANK: u64 = 0xFF;
pub const SECOND_RANK: u64 = 0xFF << 8;
pub const THIRD_RANK: u64 = 0xFF << (8 * 2);
pub const FOURTH_RANK: u64 = 0xFF << (8 * 3);
pub const FIFTH_RANK: u64 = 0xFF << (8 * 4);
pub const SIXTH_RANK: u64 = 0xFF << (8 * 5);
pub const SEVENTH_RANK: u64 = 0xFF << (8 * 6);
pub const EIGTH_RANK: u64 = 0xFF << (8 * 7);

pub const A_FILE: u64 = 0x0101010101010101;
pub const B_FILE: u64 = 0x0101010101010101 << 1;
pub const C_FILE: u64 = 0x0101010101010101 << 2;
pub const D_FILE: u64 = 0x0101010101010101 << 3;
pub const E_FILE: u64 = 0x0101010101010101 << 4;
pub const F_FILE: u64 = 0x0101010101010101 << 5;
pub const G_FILE: u64 = 0x0101010101010101 << 6;
pub const H_FILE: u64 = 0x0101010101010101 << 7;

pub const LIGHT_DIAGONAL: u64 = 0x0102040810204080;
pub const DARK_DIAGONAL: u64 = 0x8040201008040201;

pub const DARK_SQUARES: u64 = 0xAA55AA55AA55AA55;
pub const LIGHT_SQUARES: u64 = 0x55AA55AA55AA55AA;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct BitBoard {
    pub bitboard: u64,
}

impl BitBoard {
    #[inline]
    pub fn new(bitboard: u64) -> BitBoard {
        BitBoard { bitboard }
    }

    #[inline]
    pub fn new_coordinate_from_bit_index(bit_index: usize) -> Option<BitBoard> {
        if bit_index < 64 {
            Some(BitBoard::new(1 << bit_index))
        } else {
            None
        }
    }

    pub fn new_coordinate_from_string(loc: &str) -> Option<BitBoard> {
        let mut coord = loc.chars();

        let col = match coord.next()? {
            'a' => 0,
            'b' => 1,
            'c' => 2,
            'd' => 3,
            'e' => 4,
            'f' => 5,
            'g' => 6,
            'h' => 7,
            'A' => 0,
            'B' => 1,
            'C' => 2,
            'D' => 3,
            'E' => 4,
            'F' => 5,
            'G' => 6,
            'H' => 7,
            _ => return None,
        };
        let row = match coord.next()? {
            '1' => 0,
            '2' => 1,
            '3' => 2,
            '4' => 3,
            '5' => 4,
            '6' => 5,
            '7' => 6,
            '8' => 7,
            _ => return None,
        };

        if coord.next().is_some() {
            return None;
        }

        BitBoard::new_coordinate_from_bit_index(8 * row + col)
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.bitboard == 0
    }

    #[inline]
    pub fn get_lowest_bit_as_col_row(&self) -> Option<(usize, usize)> {
        let i = self.bitboard.tzcnt() as usize;
        if i == 64 {
            None
        } else {
            Some((i & 7, i >> 3))
        }
    }

    #[inline]
    pub fn go_up(&self) -> Self {
        Self {
            bitboard: self.bitboard << 8,
        }
    }

    #[inline]
    pub fn go_down(&self) -> Self {
        Self {
            bitboard: self.bitboard >> 8,
        }
    }

    #[inline]
    pub fn go_right(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !H_FILE) << 1,
        }
    }

    #[inline]
    pub fn go_left(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !A_FILE) >> 1,
        }
    }

    #[inline]
    pub fn go_ur(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !H_FILE) << 9,
        }
    }

    #[inline]
    pub fn go_ul(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !A_FILE) << 7,
        }
    }

    #[inline]
    pub fn go_dr(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !H_FILE) >> 7,
        }
    }

    #[inline]
    pub fn go_dl(&self) -> Self {
        Self {
            bitboard: (self.bitboard & !A_FILE) >> 9,
        }
    }

    pub fn potential_knights_moves(&self) -> BitBoard {
        let up = self.go_up();
        let down = self.go_down();
        let left = self.go_left();
        let right = self.go_right();

        up.go_ul()
            | up.go_ur()
            | down.go_dl()
            | down.go_dr()
            | left.go_ul()
            | left.go_dl()
            | right.go_ur()
            | right.go_dr()
    }

    pub fn get_set_bits_as_vector(mut self) -> Vec<BitBoard> {
        let mut v = Vec::new();
        while !self.is_empty() {
            let blsi = self.blsi();
            v.push(blsi);
            self ^= blsi;
        }
        v
    }

    #[inline]
    pub fn get_popcnt(&self) -> u64 {
        self.bitboard.popcnt()
    }

    #[inline]
    pub fn isolate_highest_set_bit(&self) -> Self {
        Self {
            bitboard: 1 << (63 - self.bitboard.lzcnt()),
        }
    }

    #[inline]
    pub fn get_set_bit(&self) -> usize {
        self.bitboard.tzcnt() as usize
    }
}

impl Andn for BitBoard {
    #[inline]
    fn andn(self, y: Self) -> Self {
        Self {
            bitboard: y.bitboard.andn(self.bitboard),
        }
    }
}

impl Tzcnt for BitBoard {
    #[inline]
    fn tzcnt(self) -> Self {
        Self {
            bitboard: self.bitboard.tzcnt(),
        }
    }
}

impl Blsi for BitBoard {
    #[inline]
    fn blsi(self) -> Self {
        Self {
            bitboard: self.bitboard.blsi(),
        }
    }
}

impl_op_ex!(^ |a: &BitBoard, b: &BitBoard| -> BitBoard { BitBoard { bitboard: a.bitboard ^ b.bitboard } });
impl_op_ex!(&|a: &BitBoard, b: &BitBoard| -> BitBoard {
    BitBoard {
        bitboard: a.bitboard & b.bitboard,
    }
});
impl_op_ex!(| |a: &BitBoard, b: &BitBoard| -> BitBoard { BitBoard { bitboard: a.bitboard | b.bitboard } });
impl_op_ex!(+ |a: &BitBoard, b: &BitBoard| -> BitBoard { BitBoard { bitboard: a.bitboard + b.bitboard } });
impl_op_ex!(-|a: &BitBoard, b: &BitBoard| -> BitBoard {
    BitBoard {
        bitboard: a.bitboard - b.bitboard,
    }
});
impl_op_ex!(>> |a: &BitBoard, b: &usize| -> BitBoard { BitBoard { bitboard: a.bitboard >> b } });
impl_op_ex!(<< |a: &BitBoard, b: &usize| -> BitBoard { BitBoard { bitboard: a.bitboard << b } });
impl_op_ex!(^= |a: &mut BitBoard, b: &BitBoard| { a.bitboard ^= b.bitboard });
impl_op_ex!(&= |a: &mut BitBoard, b: &BitBoard| { a.bitboard &= b.bitboard });
impl_op_ex!(|= |a: &mut BitBoard, b: &BitBoard| { a.bitboard |= b.bitboard });
impl_op_ex!(+= |a: &mut BitBoard, b: &BitBoard| { a.bitboard += b.bitboard });
impl_op_ex!(-= |a: &mut BitBoard, b: &BitBoard| { a.bitboard -= b.bitboard });
impl_op_ex!(>>= |a: &mut BitBoard, b: &usize| { a.bitboard >>= b });
impl_op_ex!(<<= |a: &mut BitBoard, b: &usize| { a.bitboard <<= b });
impl_op_ex!(!|a: &BitBoard| -> BitBoard { BitBoard::new(!a.bitboard) });

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn xor() {
        let x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        assert_eq!((x ^ y), BitBoard::new(0b1010));
    }
    #[test]
    fn and() {
        let x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        assert_eq!((x & y), BitBoard::new(0b0100));
    }
    #[test]
    fn or() {
        let x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        assert_eq!((x | y), BitBoard::new(0b1110));
    }
    #[test]
    fn plus() {
        let x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        assert_eq!((x + y), BitBoard::new(0b10010));
    }
    #[test]
    fn minus() {
        let x = BitBoard::new(0b0111);
        let y = BitBoard::new(0b1100);
        assert_eq!((y - x), BitBoard::new(0b0101));
    }

    #[test]
    fn shr() {
        assert_eq!(BitBoard::new(0b0110) >> 1, BitBoard::new(0b0011))
    }

    #[test]
    fn shl() {
        assert_eq!(BitBoard::new(0b0110) << 1, BitBoard::new(0b1100))
    }

    #[test]
    fn xorassign() {
        let mut x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        x ^= y;
        assert_eq!(x, BitBoard::new(0b1010));
    }

    #[test]
    fn orassign() {
        let mut x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        x |= y;
        assert_eq!(x, BitBoard::new(0b1110));
    }

    #[test]
    fn andassign() {
        let mut x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        x &= y;
        assert_eq!(x, BitBoard::new(0b0100));
    }

    #[test]
    fn plusassign() {
        let mut x = BitBoard::new(0b0110);
        let y = BitBoard::new(0b1100);
        x += y;
        assert_eq!(x, BitBoard::new(0b10010));
    }

    #[test]
    fn minusassign() {
        let x = BitBoard::new(0b0111);
        let mut y = BitBoard::new(0b1100);
        y -= x;
        assert_eq!(y, BitBoard::new(0b0101));
    }

    #[test]
    fn shrassign() {
        let mut x = BitBoard::new(0b0110);
        x >>= 1;
        assert_eq!(x, BitBoard::new(0b0011))
    }

    #[test]
    fn shlassign() {
        let mut x = BitBoard::new(0b0110);
        x <<= 1;
        assert_eq!(x, BitBoard::new(0b1100))
    }

    #[test]
    fn logicalnot() {
        assert_eq!(!BitBoard::new(0b0110), BitBoard::new(0xFFFFFFFFFFFFFFF9))
    }

    #[test]
    fn new_coordinate_from_bit_index_test() {
        assert_eq!(
            BitBoard::new_coordinate_from_bit_index(3),
            Some(BitBoard::new(0b1000))
        );
        assert_eq!(BitBoard::new_coordinate_from_bit_index(300), None);
    }

    #[test]
    fn new_coordinate_from_string_test() {
        assert_eq!(
            BitBoard::new_coordinate_from_string("e2"),
            Some(BitBoard::new(0x1000))
        );
        assert_eq!(BitBoard::new_coordinate_from_string("e0"), None);
        assert_eq!(BitBoard::new_coordinate_from_string("-3"), None);
        assert_eq!(BitBoard::new_coordinate_from_string("i4"), None);
        assert_eq!(BitBoard::new_coordinate_from_string("h9"), None);
    }
    #[test]
    fn is_empty() {
        assert_eq!(BitBoard::new(0).is_empty(), true);
        assert_eq!(BitBoard::new(100).is_empty(), false);
    }

    #[test]
    fn get_lowest_bit_as_col_row() {
        assert_eq!(
            BitBoard::new(0x400).get_lowest_bit_as_col_row(),
            Some((2, 1))
        );
        assert_eq!(BitBoard::new(0).get_lowest_bit_as_col_row(), None);
    }

    #[test]
    fn go_direction_test() {
        let x = BitBoard::new(0x800);
        let y = x.go_up();
        assert_eq!(y, BitBoard::new(0x80000));
        assert_ne!(x, y);
    }
}
