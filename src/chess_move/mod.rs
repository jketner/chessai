#![warn(clippy::all)]
use bitintr::*;
use std::cmp::Ordering;
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Move {
    field: u16,
}

impl Move {
    pub fn new(
        from_col: usize,
        from_row: usize,
        to_col: usize,
        to_row: usize,
        flags: u16,
    ) -> Self {
        let from = ((from_col as u16) << 3) | from_row as u16;
        let to = ((to_col as u16) << 3) | to_row as u16;
        Self {
            field: ((from & 0x3F) << 10) | ((to & 0x3F) << 4) | (flags & 0xF),
        }
    }

    pub fn get_fields(&self) -> (u16, u16, u16, u16, u16) {
        (
            self.field.bextri(0x030D),
            self.field.bextri(0x030A),
            self.field.bextri(0x0307),
            self.field.bextri(0x0304),
            self.field & 0xF,
        )
    }
}

fn coord_to_string(coord: u16) -> String {
    let mut buf = String::new();
    buf.push(match coord >> 3 {
        0 => 'a',
        1 => 'b',
        2 => 'c',
        3 => 'd',
        4 => 'e',
        5 => 'f',
        6 => 'g',
        7 => 'h',
        _ => unreachable!(),
    });
    buf.push(match coord & 7 {
        0 => '1',
        1 => '2',
        2 => '3',
        3 => '4',
        4 => '5',
        5 => '6',
        6 => '7',
        7 => '8',
        _ => unreachable!(),
    });
    buf
}

fn get_promotion_string(flags: u16) -> String {
    match flags {
        0b1000 => "n".to_string(),
        0b1100 => "n".to_string(),
        0b1001 => "b".to_string(),
        0b1101 => "b".to_string(),
        0b1010 => "r".to_string(),
        0b1110 => "r".to_string(),
        0b1011 => "q".to_string(),
        0b1111 => "q".to_string(),
        _ => "".to_string(),
    }
}

impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}{}",
            coord_to_string(self.field.bextri(0x060A)),
            coord_to_string(self.field.bextri(0x0604)),
            get_promotion_string(self.field & 0xF)
        )
    }
}

impl PartialOrd for Move {
    fn partial_cmp(&self, rhs: &Move) -> Option<Ordering> {
        Some(self.cmp(rhs))
    }
}

impl Ord for Move {
    fn cmp(&self, rhs: &Move) -> Ordering {
        (self.field & 0b1111).cmp(&(rhs.field & 0b1111))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn display() {
        let m = Move {
            field: 0b0101000010010000,
        };
        assert_eq!(format!("{}", m), "c5b2");
    }

    #[test]
    fn new() {
        let m = Move {
            field: 0b0101000010010111,
        };
        let m1 = Move::new(2, 4, 1, 1, 7);
        println!("{:b}", m1.field);
        assert_eq!(m, m1);
    }
}
