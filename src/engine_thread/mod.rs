#![warn(clippy::all)]
use crate::board::Board;
use crate::chess_move::Move;
use std::ops::*;
use std::sync::mpsc::Receiver;
use std::sync::Arc;
use std::thread;
use std::time::{Duration, Instant};

#[derive(Debug, Copy, Clone)]
struct TableEntry {
    zobrist_key: u64,
    best_move: Move,
    node_and_depth: u8,
    score: i16,
}

const TABLE_SIZE: usize = 1 << 25;

impl TableEntry {
    #[inline]
    fn new(
        zobrist_key: u64,
        best_move: Move,
        score: i16,
        node_and_depth: u8,
    ) -> Self {
        Self {
            zobrist_key,
            best_move,
            score,
            node_and_depth,
        }
    }

    #[inline]
    fn get_fields(&self) -> (u64, Move, i16, u8) {
        (
            self.zobrist_key,
            self.best_move,
            self.score,
            self.node_and_depth,
        )
    }
}

struct TranspositionTable {
    table: Vec<Option<TableEntry>>,
}

impl TranspositionTable {
    fn new() -> Self {
        let tmp_table = vec![None; TABLE_SIZE];

        Self { table: tmp_table }
    }

    #[inline]
    fn get_entry(&self, key: u64) -> Option<TableEntry> {
        self.table[key as usize & (TABLE_SIZE - 1)]
    }

    #[inline]
    fn insert(&mut self, value: TableEntry) {
        self.table[value.get_fields().0 as usize & (TABLE_SIZE - 1)] =
            Some(value);
    }

    fn hash_full(&self) -> usize {
        self.table.iter().filter(|e| e.is_some()).count() * 1000 / TABLE_SIZE
    }
}

struct EngineResult {
    best_variation: Vec<Move>,
    score: i16,
}

impl EngineResult {
    fn new(best_variation: Vec<Move>, score: i16) -> Self {
        Self {
            best_variation,
            score,
        }
    }

    fn copy(&self) -> Self {
        let mut other_array = Vec::new();
        for m in &self.best_variation {
            other_array.push(*m);
        }

        Self {
            best_variation: other_array,
            score: self.score,
        }
    }

    #[inline]
    fn push(&mut self, m: Move) {
        self.best_variation.push(m);
    }

    fn variation_to_string(&self) -> String {
        let mut buf = String::new();

        for m in self.best_variation.iter().rev() {
            buf.push_str(&format!("{} ", m));
        }

        buf
    }

    #[inline]
    fn variation_is_empty(&self) -> bool {
        self.best_variation.is_empty()
    }

    #[inline]
    fn get_first_move(&self) -> Move {
        if !self.best_variation.is_empty() {
            self.best_variation[self.best_variation.len() - 1]
        } else {
            println!("{}", self.score);
            panic!()
        }
    }

    fn mate_increment(&mut self) {
        if self.score == i16::MAX || self.score == i16::MIN {
            panic!();
        }
        if self.score > 0 {
            self.score += 1;
        } else {
            self.score -= 1;
        }
    }

    #[inline]
    fn get_score(&self) -> i16 {
        self.score
    }
}

impl Neg for EngineResult {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            best_variation: self.best_variation,
            score: -self.score,
        }
    }
}

pub struct EngineThread {
    rx: Receiver<Arc<String>>,
    board: Board,
    move_start: Instant,
    move_time: Duration,
    best_move: EngineResult,
    transposition_table: TranspositionTable,
    nodes_searched: u128,
}

impl EngineThread {
    fn alpha_beta(&mut self) -> Result<(), ()> {
        self.move_start = Instant::now();
        self.nodes_searched = 0;
        for depth in 2.. {
            self.nodes_searched += 1;
            let mut children = self.board.get_legal_moves();

            let entry = self
                .transposition_table
                .get_entry(self.board.get_zobrist_key());

            let mut previous_score = None;
            if let Some(entry_key) = entry.map(|e| e.get_fields().0) {
                if entry_key == self.board.get_zobrist_key() {
                    let prev_best_move = entry.map(|e| e.get_fields().1);
                    previous_score = entry.map(|e| e.get_fields().2);
                    if let Some(m) = prev_best_move {
                        children.swap_to_front(m);
                    }
                }
            }

            let mut depth_best = EngineResult::new(Vec::new(), -i16::MAX);

            for (i, child) in children.enumerate() {
                self.board.make_move(child);
                let mut best_var = self.table_or_search(
                    depth - 1,
                    depth_best.get_score(),
                    i16::MAX,
                    i == 0,
                )?;
                best_var.push(child);
                self.board.unmake_move(child);
                if best_var.get_score() > depth_best.get_score() || i == 0 {
                    depth_best = best_var;
                }
                if previous_score.is_none()
                    || depth_best.get_score() > previous_score.unwrap() - 300
                {
                    self.best_move = depth_best.copy();
                }

                let since_start =
                    Instant::now().duration_since(self.move_start).as_millis();

                if since_start > 500 {
                    println!(
                        "info currmove {} currmovenumber {} depth {} pv {} score cp {} nodes {} nps {} time {} hashfull {}",
                        child,
                        i + 1,
                        depth,
                        self.best_move.variation_to_string(),
                        self.best_move.get_score(),
                        self.nodes_searched,
                        self.nodes_searched * 1000 / since_start,
                        since_start,
                        self.transposition_table.hash_full(),
                    );
                }
            }
            self.best_move = depth_best.copy();
            let first_entry = TableEntry::new(
                self.board.get_zobrist_key(),
                self.best_move.get_first_move(),
                self.best_move.get_score(),
                depth,
            );
            self.transposition_table.insert(first_entry);
        }
        Ok(())
    }

    fn table_or_search(
        &mut self,
        depth: u8,
        alpha: i16,
        beta: i16,
        mut is_pv: bool,
    ) -> Result<EngineResult, ()> {
        let entry = self
            .transposition_table
            .get_entry(self.board.get_zobrist_key());
        let mut best_move = None;
        if let Some(e) = entry {
            let (entry_key, entry_best_move, score, node_and_depth) =
                e.get_fields();
            if entry_key == self.board.get_zobrist_key() {
                let node_type = node_and_depth >> 6;
                if (node_and_depth & 0b00111111) >= depth
                    && (node_type == 0b11
                        || (node_type == 0b10 && score < alpha)
                        || (node_type == 0b01 && score > beta))
                {
                    return Ok(EngineResult::new(vec![entry_best_move], score));
                }
                best_move = Some(entry_best_move);
                if (node_and_depth >> 6 == 0b10)
                    || (node_and_depth >> 6 == 0b01)
                {
                    is_pv = false;
                } else {
                    is_pv = true;
                }
            }
        }
        let best_result = if is_pv {
            -self.alpha_beta_inner(depth, -beta, -alpha, best_move)?
        } else {
            let mut tmp =
                -self.alpha_beta_inner(depth, -alpha - 1, -alpha, best_move)?;
            if tmp.get_score() > alpha && tmp.get_score() < beta {
                tmp =
                    -self.alpha_beta_inner(depth, -beta, -alpha, best_move)?;
            }
            tmp
        };
        let depth_and_u8 = if best_result.get_score() >= beta {
            //fail high/beta cutoff/cut node
            0b01000000 | (depth & 0b00111111)
        } else if best_result.score <= alpha {
            //fail low/alpha cutoff/all-node
            0b10000000 | (depth & 0b00111111)
        } else {
            //pv-node/ exact
            0b11000000 | (depth & 0b00111111)
        };
        if !best_result.variation_is_empty() {
            let new_entry = TableEntry::new(
                self.board.get_zobrist_key(),
                best_result.get_first_move(),
                best_result.get_score(),
                depth_and_u8,
            );
            self.transposition_table.insert(new_entry);
        } else {
            let new_entry = TableEntry::new(
                self.board.get_zobrist_key(),
                Move::new(0, 0, 0, 0, 0),
                best_result.get_score(),
                depth_and_u8,
            );
            self.transposition_table.insert(new_entry);
        }
        Ok(best_result)
    }

    fn check_stop_condition(&self) -> Result<(), ()> {
        if let Ok(s) = self.rx.try_recv() {
            if Arc::try_unwrap(s).unwrap() == "stop" {
                println!("bestmove {}", self.best_move.get_first_move());
                return Err(());
            }
        };
        if self.move_start.elapsed() > self.move_time {
            println!("bestmove {}", self.best_move.get_first_move());
            return Err(());
        }
        Ok(())
    }

    fn quiesce(&mut self, mut alpha: i16, beta: i16) -> i16 {
        let is_in_check = self.board.is_in_check(self.board.get_king_loc());

        let children = self.board.get_legal_moves();

        if children.is_empty() {
            if is_in_check {
                return -i16::MAX / 2;
            } else {
                return 0;
            }
        }

        if !is_in_check {
            let stand_pat = self.board.evaluate();
            if stand_pat >= beta {
                return beta;
            }
            if alpha < stand_pat {
                alpha = stand_pat;
            }
        }

        for m in children {
            if !is_in_check && m.get_fields().4 & 0b0100 == 0 {
                continue;
            }

            self.board.make_move(m);
            self.nodes_searched += 1;
            let eval = -self.quiesce(-beta, -alpha);
            self.board.unmake_move(m);

            if eval >= beta {
                return beta;
            }
            if eval > alpha {
                alpha = eval;
            }
        }
        alpha
    }

    fn alpha_beta_inner(
        &mut self,
        depth: u8,
        alpha: i16,
        beta: i16,
        best_move: Option<Move>,
    ) -> Result<EngineResult, ()> {
        if depth == 0 {
            self.check_stop_condition()?;
            let eval = self
                .transposition_table
                .get_entry(self.board.get_zobrist_key());
            if let Some(eval) = eval {
                let (eval_zobrist, eval_best_move, eval_score, node_and_depth) =
                    eval.get_fields();
                if eval_zobrist == self.board.get_zobrist_key()
                    && node_and_depth >> 6 == 0b11
                {
                    return Ok(EngineResult::new(
                        vec![eval_best_move],
                        eval_score,
                    ));
                } else {
                    return Ok(EngineResult::new(
                        Vec::new(),
                        self.quiesce(alpha, beta),
                    ));
                }
            } else {
                return Ok(EngineResult::new(
                    Vec::new(),
                    self.quiesce(alpha, beta),
                ));
            }
        }

        let mut children = self.board.get_legal_moves();

        if children.is_empty() {
            if self.board.is_in_check(self.board.get_king_loc()) {
                return Ok(EngineResult::new(Vec::new(), -i16::MAX / 2));
            }
            return Ok(EngineResult::new(Vec::new(), 0));
        }

        if let Some(m) = best_move {
            children.swap_to_front(m);
        }

        let mut depth_best =
            EngineResult::new(vec![children.get_first()], alpha);

        for (i, child) in children.enumerate() {
            self.board.make_move(child);
            self.nodes_searched += 1;
            let mut best_var = self.table_or_search(
                depth - 1,
                depth_best.get_score(),
                beta,
                i == 0,
            )?;
            best_var.push(child);
            self.board.unmake_move(child);
            if best_var.get_score() > depth_best.get_score() {
                if best_var.get_score() >= beta {
                    return Ok(best_var);
                }
                if best_var.get_score() >= i16::MAX / 2
                    || best_var.get_score() <= -i16::MAX / 2
                {
                    best_var.mate_increment();
                }
                depth_best = best_var.copy();
            }
        }
        Ok(depth_best)
    }

    fn initialize_from_fen(&mut self, fen: &str) {
        let fen_end = match fen.find("moves") {
            None => fen.len() - 1,
            Some(i) => i - 1,
        };
        self.board.initialize(&fen[13..fen_end]).unwrap();
        if fen_end != fen.len() - 1 {
            fen[(fen_end + 6)..fen.len() - 1]
                .split_whitespace()
                .for_each(|m| {
                    let converted = self
                        .board
                        .get_legal_moves()
                        .find(|l| l.to_string() == m)
                        .or_else(|| {
                            self.board.print_board();
                            println!("{}", m);
                            None
                        })
                        .unwrap();
                    self.board.make_move(converted);
                })
        }
    }

    pub fn spawn(rx: Receiver<Arc<String>>) {
        thread::spawn(move || {
            let mut t = Self {
                board: Board::new(),
                rx,
                move_start: Instant::now(),
                move_time: Duration::from_millis(30000),
                best_move: EngineResult::new(Vec::new(), i16::MIN),
                transposition_table: TranspositionTable::new(),
                nodes_searched: 0,
            };
            t.board.initialize("startpos").unwrap();
            loop {
                let sent_signal = match t.rx.try_recv() {
                    Ok(s) => Arc::try_unwrap(s).unwrap(),
                    _ => "".to_string(),
                };
                if sent_signal.starts_with("go") {
                    let mut s_iter = sent_signal.split_whitespace();
                    while let Some(word) = s_iter.next() {
                        if word == "movetime" {
                            t.move_time = Duration::from_millis(
                                s_iter.next().unwrap().parse().unwrap(),
                            );
                        }
                    }
                    let _ = t.alpha_beta();
                } else if sent_signal.starts_with("position ") {
                    t.initialize_from_fen(&sent_signal);
                } else if sent_signal == "ucinewgame " {
                    t.board = Board::new();
                    t.move_time = Duration::from_millis(30000);
                    t.transposition_table = TranspositionTable::new();
                }
            }
        });
    }
}
