#![warn(clippy::all)]
use std::env;

fn main() -> Result<(), String> {
    let args: Vec<_> = env::args().collect();
    if args.len() == 1 {
        alpha_beta_rust::universal_chess_interface();
    } else if args[1] == "d" && args.len() == 4 {
        let _ = alpha_beta_rust::divide(
            &args[2],
            args[3].parse::<usize>().map_err(|e| e.to_string())?,
        )?;
    } else if args[1] == "s" && args.len() == 4 {
        alpha_beta_rust::stockfish_perft(
            &args[2],
            args[3].parse::<usize>().map_err(|e| e.to_string())?,
        );
    } else {
        unimplemented!()
    }
    Ok(())
}
