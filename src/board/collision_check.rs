use crate::chess_move::coordinate::Coordinate;

#[derive(Clone, Copy)]
pub struct CollisionCheck {
#[allow(dead_code)]
    loc: Coordinate,
#[allow(dead_code)]
    direction: fn(&Coordinate) -> Coordinate,
#[allow(dead_code)]
    collided: bool
}

impl CollisionCheck {
#[allow(dead_code)]
    pub fn from(loc: Coordinate, direction: fn(&Coordinate) -> Coordinate) -> Self {
        CollisionCheck {
            loc: loc,
            direction: direction,
            collided: false
        }
    }

#[allow(dead_code)]
    pub fn is_collided(&self) -> bool {
        self.collided
    }

#[allow(dead_code)]
    pub fn set_collided(&mut self) {
        self.collided = true;
    }

#[allow(dead_code)]
    pub fn get_loc(&self) -> Coordinate {
        self.loc
    }

#[allow(dead_code)]
    pub fn move_coordinate(&mut self) {
        self.loc = (self.direction)(&self.loc);
    }
}
