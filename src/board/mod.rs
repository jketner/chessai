#![warn(clippy::all)]
use std::cmp;
extern crate rand;
extern crate rand_xoshiro;
use crate::piece::{self, kind::Kind, player::Player, Piece};
use legal_moves::LegalMoves;
use rand::prelude::*;
use rand_xoshiro::rand_core::SeedableRng;
use rand_xoshiro::Xoshiro512PlusPlus;
mod boards;
mod legal_moves;
use super::bitboard::*;
use super::chess_move::Move;
use boards::Boards;

const PROMOTION: u16 = 0b1000;
const CAPTURE: u16 = 0b0100;
const SPECIAL_1: u16 = 0b0010;
const SPECIAL_0: u16 = 0b0001;

#[derive(PartialEq, Eq, Copy, Clone)]
enum CastleRight {
    WhiteQueen,
    WhiteKing,
    BlackQueen,
    BlackKing,
}

struct Zobrist {
    mutators: [u64; 781],
    key: u64,
}

impl Zobrist {
    #[inline]
    fn new_board_position(&mut self) {
        self.key = 0;
    }

    #[inline]
    pub fn get_key(&self) -> u64 {
        self.key
    }

    pub fn new() -> Self {
        let mut mutators = [0; 781];
        let mut rng = Xoshiro512PlusPlus::seed_from_u64(0x74502dacf309cf49);
        rng.fill(&mut mutators[..]);
        Self { key: 0, mutators }
    }

    #[inline]
    pub fn xor_piece_loc(&mut self, piece: Piece, loc: BitBoard) {
        let piece_index = match piece {
            Piece {
                color: Player::White,
                kind: Kind::Pawn,
            } => 0,
            Piece {
                color: Player::White,
                kind: Kind::Knight,
            } => 1,
            Piece {
                color: Player::White,
                kind: Kind::Bishop,
            } => 2,
            Piece {
                color: Player::White,
                kind: Kind::Rook,
            } => 3,
            Piece {
                color: Player::White,
                kind: Kind::Queen,
            } => 4,
            Piece {
                color: Player::White,
                kind: Kind::King,
            } => 5,
            Piece {
                color: Player::Black,
                kind: Kind::Pawn,
            } => 6,
            Piece {
                color: Player::Black,
                kind: Kind::Knight,
            } => 7,
            Piece {
                color: Player::Black,
                kind: Kind::Bishop,
            } => 8,
            Piece {
                color: Player::Black,
                kind: Kind::Rook,
            } => 9,
            Piece {
                color: Player::Black,
                kind: Kind::Queen,
            } => 10,
            Piece {
                color: Player::Black,
                kind: Kind::King,
            } => 11,
        };
        self.key ^= self.mutators[piece_index * 64 + loc.get_set_bit() + 13];
    }

    #[inline]
    pub fn xor_en_passant(&mut self, file: u16) {
        self.key ^= self.mutators[file as usize];
    }

    #[inline]
    pub fn xor_castle_rights(&mut self, right: usize) {
        self.key ^= self.mutators[right + 8];
    }

    #[inline]
    pub fn xor_player(&mut self) {
        self.key ^= self.mutators[12];
    }
}

pub struct Board {
    board: Boards,
    player: Player,
    castle_right: [bool; 4],
    en_passant_square: BitBoard,
    halfmove_clock: u8,
    fullmove_counter: u8,
    #[allow(clippy::type_complexity)]
    stack_previous_unrecoverables:
        Vec<([bool; 4], BitBoard, u8, u8, Option<Piece>)>,
    zobrist: Zobrist,
}

impl Board {
    pub fn zobrist_diagnosis(&self, key: u64) {
        if key == self.zobrist.key {
            println!("wtf");
            panic!();
        }
        for (i, mutator) in self.zobrist.mutators.iter().enumerate() {
            if self.zobrist.key ^ mutator == key {
                println!("mutators_index: {}", i);
                panic!();
            }
        }
        self.zobrist
            .mutators
            .iter()
            .enumerate()
            .flat_map(|(i, mutator)| {
                self.zobrist
                    .mutators
                    .iter()
                    .enumerate()
                    .map(move |(j, nutator)| ((i, j), (mutator, nutator)))
            })
            .for_each(|((i, j), (mutator, nutator))| {
                if self.zobrist.key ^ mutator ^ nutator == key {
                    println!("mutators_index: {} {}", i, j);
                    panic!();
                }
            })
    }

    pub fn print_board(&self) {
        println!("\n-------------------------");
        for row_index in (0..8).rev() {
            print!("|");
            for col_index in 0..8 {
                let loc = BitBoard::new_coordinate_from_bit_index(
                    col_index + 8 * row_index,
                )
                .unwrap();
                let mut found = false;
                for piece in piece::LIST.iter() {
                    if (loc & !self.board[*piece]).is_empty() {
                        print!("{}", piece.to_string());
                        found = true;
                    }
                }
                if !found {
                    print!("  |")
                }
            }
            println!("\n-------------------------");
        }
    }

    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Board {
            board: Boards::new(),
            player: Player::White,
            castle_right: [true; 4],
            en_passant_square: BitBoard::new(0),
            halfmove_clock: 0,
            fullmove_counter: 0,
            stack_previous_unrecoverables: Vec::new(),
            zobrist: Zobrist::new(),
        }
    }
    pub fn initialize<'a>(
        &mut self,
        mut fen_string: &str,
    ) -> Result<(), &'a str> {
        if fen_string == "startpos" {
            fen_string =
                "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0";
        };

        let fen: Vec<_> = fen_string.split(' ').collect();
        self.board = Boards::new();
        self.zobrist.new_board_position();

        let row_strings: Vec<_> = fen[0].split('/').enumerate().collect();
        if row_strings.len() != 8 {
            return Err("Invalid FEN string");
        }
        for (row_index, row) in row_strings {
            let mut col_index = 0;
            let mut letters = row.chars();
            while col_index < 8 {
                let loc = BitBoard::new_coordinate_from_bit_index(
                    col_index + 8 * (7 - row_index),
                )
                .unwrap();
                match letters.next() {
                    Some('K') => self.toggle_square(
                        Piece::from(Player::White, Kind::King),
                        loc,
                    ),
                    Some('Q') => self.toggle_square(
                        Piece::from(Player::White, Kind::Queen),
                        loc,
                    ),
                    Some('R') => self.toggle_square(
                        Piece::from(Player::White, Kind::Rook),
                        loc,
                    ),
                    Some('B') => self.toggle_square(
                        Piece::from(Player::White, Kind::Bishop),
                        loc,
                    ),
                    Some('N') => self.toggle_square(
                        Piece::from(Player::White, Kind::Knight),
                        loc,
                    ),
                    Some('P') => self.toggle_square(
                        Piece::from(Player::White, Kind::Pawn),
                        loc,
                    ),
                    Some('k') => self.toggle_square(
                        Piece::from(Player::Black, Kind::King),
                        loc,
                    ),
                    Some('q') => self.toggle_square(
                        Piece::from(Player::Black, Kind::Queen),
                        loc,
                    ),
                    Some('r') => self.toggle_square(
                        Piece::from(Player::Black, Kind::Rook),
                        loc,
                    ),
                    Some('b') => self.toggle_square(
                        Piece::from(Player::Black, Kind::Bishop),
                        loc,
                    ),
                    Some('n') => self.toggle_square(
                        Piece::from(Player::Black, Kind::Knight),
                        loc,
                    ),
                    Some('p') => self.toggle_square(
                        Piece::from(Player::Black, Kind::Pawn),
                        loc,
                    ),
                    Some(c) if c >= '1' && c <= '9' => {
                        col_index += c as usize - '1' as usize
                    }
                    _ => {
                        return Err("Invalid FEN string, board is not correct")
                    }
                };
                col_index += 1;
            }
        }

        self.player = match fen[1] {
            "w" => Player::White,
            "b" => {
                self.zobrist.xor_player();
                Player::Black
            }
            _ => return Err("Invalid FEN string, second field must be w or b"),
        };

        self.castle_right = [false; 4];
        if fen[2] != "-" {
            let mut index = 0;
            self.castle_right[0] = if fen[2][index..].starts_with('K') {
                self.zobrist.xor_castle_rights(0);
                index += 1;
                true
            } else {
                false
            };
            self.castle_right[1] = if fen[2][index..].starts_with('Q') {
                self.zobrist.xor_castle_rights(1);
                index += 1;
                true
            } else {
                false
            };
            self.castle_right[2] = if fen[2][index..].starts_with('k') {
                self.zobrist.xor_castle_rights(2);
                index += 1;
                true
            } else {
                false
            };
            self.castle_right[3] = if fen[2][index..].starts_with('q') {
                self.zobrist.xor_castle_rights(3);
                index += 1;
                true
            } else {
                false
            };
            if !fen[2][index..].is_empty() {
                return Err("Invalid FEN string: castle rights invalid");
            }
        }

        if fen[3] == "-" {
            self.en_passant_square = BitBoard::new(0);
        } else if fen[3].len() != 2 {
            return Err("Invalid FEN string: en_passant incorrect length");
        } else {
            self.en_passant_square =
                BitBoard::new_coordinate_from_string(fen[3])
                    .ok_or_else(|| "Invalid en_passant_square")?;
            let mut en_passant_file_index = 0;
            while (BitBoard::new(A_FILE << en_passant_file_index)
                & self.en_passant_square)
                .is_empty()
            {
                en_passant_file_index += 1;
            }
            self.zobrist.xor_en_passant(en_passant_file_index);
        }

        self.halfmove_clock = fen[4]
            .parse()
            .map_err(|_e| "Parse Error on halfmove_clock")?;

        self.fullmove_counter = fen[5]
            .parse()
            .map_err(|_e| "Parse Error on fullmove_counter")?;
        Ok(())
    }

    #[inline]
    fn toggle_square(&mut self, p: Piece, loc: BitBoard) {
        self.board[p] ^= loc;
        self.zobrist.xor_piece_loc(p, loc);
    }

    #[inline]
    fn toggle_castle_right(&mut self, right: CastleRight) {
        let right_index = match right {
            CastleRight::WhiteKing => 0,
            CastleRight::WhiteQueen => 1,
            CastleRight::BlackKing => 2,
            CastleRight::BlackQueen => 3,
        };
        self.zobrist.xor_castle_rights(right_index);
        self.castle_right[right_index] = !self.castle_right[right_index];
    }

    #[inline]
    fn get_castle_right(&self, right: CastleRight) -> bool {
        self.castle_right[match right {
            CastleRight::WhiteKing => 0,
            CastleRight::WhiteQueen => 1,
            CastleRight::BlackKing => 2,
            CastleRight::BlackQueen => 3,
        }]
    }

    #[inline]
    pub fn make_move(&mut self, m: Move) {
        let (from_col, from_row, to_col, to_row, flags) = m.get_fields();
        let from = BitBoard::new(1 << (from_col + from_row * 8));
        let to = BitBoard::new(1 << (to_col + to_row * 8));
        let from_piece = *piece::LIST
            .iter()
            .find(|&&p| !(self.board[p] & from).is_empty())
            .unwrap();
        let mut capture_piece: Option<Piece> = None;
        if flags & CAPTURE != 0 {
            if flags & PROMOTION != 0 || flags & SPECIAL_0 == 0 {
                capture_piece = piece::LIST
                    .iter()
                    .find(|&&p| !(self.board[p] & to).is_empty())
                    .copied();
                self.toggle_square(capture_piece.unwrap(), to);
            } else {
                capture_piece = Some(Piece::from(!self.player, Kind::Pawn));
                let ep_to = match self.player {
                    Player::Black => BitBoard::new(1 << (to_col + 3 * 8)),
                    Player::White => BitBoard::new(1 << (to_col + 4 * 8)),
                };
                self.toggle_square(capture_piece.unwrap(), ep_to);
            }
        }
        self.stack_previous_unrecoverables.push((
            self.castle_right,
            self.en_passant_square,
            self.halfmove_clock,
            self.fullmove_counter,
            capture_piece,
        ));
        if flags & CAPTURE != 0 {
            self.halfmove_clock = 0;
        } else {
            self.halfmove_clock += 1;
        }
        if self.en_passant_square != BitBoard::new(0) {
            let mut file_index = 0;
            while (BitBoard::new(A_FILE << file_index) & self.en_passant_square)
                .is_empty()
            {
                file_index += 1;
            }
            self.zobrist.xor_en_passant(file_index);
        }
        self.en_passant_square = if flags == SPECIAL_0 {
            self.zobrist.xor_en_passant(to_col);
            match self.player {
                Player::Black => BitBoard::new(1 << (to_col + 5 * 8)),
                Player::White => BitBoard::new(1 << (to_col + 2 * 8)),
            }
        } else {
            BitBoard::new(0)
        };
        if flags == SPECIAL_1 {
            match self.player {
                Player::White => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(7).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(5).unwrap(),
                    );
                }
                Player::Black => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(63).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(61).unwrap(),
                    );
                }
            }
        }
        if flags == SPECIAL_1 | SPECIAL_0 {
            match self.player {
                Player::White => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(3).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(0).unwrap(),
                    );
                }
                Player::Black => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(56).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(59).unwrap(),
                    );
                }
            }
        }
        if flags & PROMOTION != 0 {
            let promote_piece = match flags & 0b11 {
                0 => Piece::from(self.player, Kind::Knight),
                1 => Piece::from(self.player, Kind::Bishop),
                2 => Piece::from(self.player, Kind::Rook),
                3 => Piece::from(self.player, Kind::Queen),
                _ => unreachable!(),
            };
            self.toggle_square(promote_piece, to);
            self.toggle_square(from_piece, from);
        } else {
            self.toggle_square(from_piece, to);
            self.toggle_square(from_piece, from);
        };
        if from_piece.kind == Kind::King {
            match self.player {
                Player::Black => {
                    [CastleRight::BlackQueen, CastleRight::BlackKing]
                        .iter()
                        .for_each(|&r| {
                            if self.get_castle_right(r) {
                                self.toggle_castle_right(r)
                            }
                        });
                }
                Player::White => {
                    [CastleRight::WhiteQueen, CastleRight::WhiteKing]
                        .iter()
                        .for_each(|&r| {
                            if self.get_castle_right(r) {
                                self.toggle_castle_right(r)
                            }
                        });
                }
            }
        }
        [
            (from_piece.kind == Kind::Rook, from),
            (
                capture_piece == Some(Piece::from(!self.player, Kind::Rook)),
                to,
            ),
        ]
        .iter()
        .for_each(|(b, loc)| {
            if *b {
                [0, 7, 56, 63]
                    .iter()
                    .map(|i| {
                        BitBoard::new_coordinate_from_bit_index(*i).unwrap()
                    })
                    .zip(
                        [
                            CastleRight::WhiteQueen,
                            CastleRight::WhiteKing,
                            CastleRight::BlackQueen,
                            CastleRight::BlackKing,
                        ]
                        .iter(),
                    )
                    .for_each(|(corner_square, castle_side)| {
                        if *loc == corner_square
                            && self.get_castle_right(*castle_side)
                        {
                            self.toggle_castle_right(*castle_side);
                        }
                    })
            }
        });
        self.player = !self.player;
        self.zobrist.xor_player();
        if self.player == Player::White {
            self.fullmove_counter += 1;
        }
    }

    #[inline]
    pub fn unmake_move(&mut self, m: Move) {
        self.player = !self.player;
        self.zobrist.xor_player();
        let (from_col, from_row, to_col, to_row, flags) = m.get_fields();
        let from = BitBoard::new(1 << (from_col + from_row * 8));
        let to = BitBoard::new(1 << (to_col + to_row * 8));
        let from_piece = if flags & PROMOTION == 0 {
            *piece::LIST
                .iter()
                .find(|&&p| !(self.board[p] & to).is_empty())
                .unwrap()
        } else {
            Piece::from(self.player, Kind::Pawn)
        };
        if !self.en_passant_square.is_empty() {
            let mut file_index = 0;
            while (BitBoard::new(A_FILE << file_index) & self.en_passant_square)
                .is_empty()
            {
                file_index += 1;
            }
            self.zobrist.xor_en_passant(file_index);
        }
        let unrecoverables = self.stack_previous_unrecoverables.pop().unwrap();
        let before_rights = self.castle_right;
        self.castle_right = unrecoverables.0;
        self.en_passant_square = unrecoverables.1;
        self.halfmove_clock = unrecoverables.2;
        self.fullmove_counter = unrecoverables.3;
        let capture_piece = unrecoverables.4;
        if !self.en_passant_square.is_empty() {
            let mut file_index = 0;
            while (BitBoard::new(A_FILE << file_index) & self.en_passant_square)
                .is_empty()
            {
                file_index += 1;
            }
            self.zobrist.xor_en_passant(file_index);
        }
        (0..=3).for_each(|i| {
            if self.castle_right[i] != before_rights[i] {
                self.zobrist.xor_castle_rights(i);
            }
        });
        if flags & CAPTURE != 0 {
            if flags & PROMOTION != 0 || flags & SPECIAL_0 == 0 {
                self.toggle_square(capture_piece.unwrap(), to);
            } else {
                let ep_to = match self.player {
                    Player::Black => BitBoard::new(1 << (to_col + 3 * 8)),
                    Player::White => BitBoard::new(1 << (to_col + 4 * 8)),
                };
                self.toggle_square(capture_piece.unwrap(), ep_to);
            }
        }
        if flags == SPECIAL_1 {
            match self.player {
                Player::White => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(5).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(7).unwrap(),
                    );
                }
                Player::Black => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(61).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(63).unwrap(),
                    );
                }
            }
        }
        if flags == SPECIAL_1 | SPECIAL_0 {
            match self.player {
                Player::White => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(0).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(3).unwrap(),
                    );
                }
                Player::Black => {
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(56).unwrap(),
                    );
                    self.toggle_square(
                        Piece::from(self.player, Kind::Rook),
                        BitBoard::new_coordinate_from_bit_index(59).unwrap(),
                    );
                }
            }
        }
        if flags & PROMOTION != 0 {
            let promote_piece = match flags & 0b11 {
                0 => Piece::from(self.player, Kind::Knight),
                1 => Piece::from(self.player, Kind::Bishop),
                2 => Piece::from(self.player, Kind::Rook),
                3 => Piece::from(self.player, Kind::Queen),
                _ => unreachable!(),
            };
            self.toggle_square(promote_piece, to);
            self.toggle_square(from_piece, from);
        } else {
            self.toggle_square(from_piece, to);
            self.toggle_square(from_piece, from);
        };
    }

    #[inline]
    pub fn get_legal_moves(&mut self) -> LegalMoves {
        let (friendly_pieces, enemy_pieces) = piece::LIST
            .iter()
            .map(|&p| (p, self.board[p]))
            .fold((BitBoard::new(0), BitBoard::new(0)), |(f, e), (p, b)| {
                if p.color == self.player {
                    (f | b, e)
                } else {
                    (f, e | b)
                }
            });
        let mut legal_moves = LegalMoves::new();
        self.pawn_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        self.knight_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        self.bishop_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        self.rook_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        self.queen_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        self.king_moves(friendly_pieces, enemy_pieces, &mut legal_moves);
        legal_moves
    }

    #[inline]
    fn would_be_in_check(&mut self, m: Move) -> bool {
        let (from_col, from_row, to_col, to_row, flags) = m.get_fields();
        let from = BitBoard::new(1 << (from_col + 8 * from_row));
        let to = BitBoard::new(1 << (to_col + 8 * to_row));
        let from_piece = *piece::LIST
            .iter()
            .find(|p| !(self.board[**p] & from).is_empty())
            .unwrap_or_else(|| {
                self.print_board();
                println!("{}", m);
                panic!()
            });
        let ep = match !self.player {
            Player::Black => BitBoard::new(1 << (to_col + 8 * 4)),
            Player::White => BitBoard::new(1 << (to_col + 8 * 3)),
        };
        let capture_piece = if flags & CAPTURE != 0 {
            let tmp = piece::LIST
                .iter()
                .find(|p| !(self.board[**p] & to).is_empty());
            if let Some(tmp) = tmp {
                self.board[*tmp] ^= to;
            } else {
                self.board[Piece::from(!self.player, Kind::Pawn)] ^= ep;
            }
            tmp
        } else {
            None
        };
        self.board[from_piece] ^= to | from;
        let keep =
            self.is_in_check(self.board[Piece::from(self.player, Kind::King)]);
        self.board[from_piece] ^= to | from;
        if flags & CAPTURE != 0 {
            if let Some(capture_piece) = capture_piece {
                self.board[*capture_piece] ^= to;
            } else {
                self.board[Piece::from(!self.player, Kind::Pawn)] ^= ep;
            }
        }
        keep
    }

    fn pawn_moves(
        &mut self,
        friendly_pieces: BitBoard,
        enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::Pawn);
        let pawns = self.board[dummy].get_set_bits_as_vector();
        for pawn in pawns {
            let all_pieces = enemy_pieces | friendly_pieces;
            let (pawn_col, pawn_row) =
                pawn.get_lowest_bit_as_col_row().unwrap();
            let one_space = pawn_direction(self.player)(&pawn);
            let (os_col, os_row) =
                one_space.get_lowest_bit_as_col_row().unwrap_or_else(|| {
                    self.print_board();
                    panic!()
                });
            let os_move = Move::new(pawn_col, pawn_row, os_col, os_row, 0);
            if (one_space & all_pieces).is_empty() {
                if os_row == 7 || os_row == 0 {
                    if !self.would_be_in_check(os_move) {
                        legal_moves.push_high_priority(Move::new(
                            pawn_col, pawn_row, os_col, os_row, PROMOTION,
                        ));
                        legal_moves.push_high_priority(Move::new(
                            pawn_col,
                            pawn_row,
                            os_col,
                            os_row,
                            PROMOTION | SPECIAL_0,
                        ));
                        legal_moves.push_high_priority(Move::new(
                            pawn_col,
                            pawn_row,
                            os_col,
                            os_row,
                            PROMOTION | SPECIAL_1,
                        ));
                        legal_moves.push_high_priority(Move::new(
                            pawn_col,
                            pawn_row,
                            os_col,
                            os_row,
                            PROMOTION | SPECIAL_1 | SPECIAL_0,
                        ));
                    }
                } else {
                    if !self.would_be_in_check(os_move) {
                        legal_moves.push_low_priority(Move::new(
                            pawn_col, pawn_row, os_col, os_row, 0,
                        ));
                    }
                    let two_space = pawn_direction(self.player)(&one_space);
                    let (ts_col, ts_row) =
                        two_space.get_lowest_bit_as_col_row().unwrap();
                    let ts_move = Move::new(
                        pawn_col, pawn_row, ts_col, ts_row, SPECIAL_0,
                    );
                    if !self.would_be_in_check(ts_move)
                        && (((os_row == 2 && self.player == Player::White)
                            && (two_space & all_pieces).is_empty())
                            || ((os_row == 5 && self.player == Player::Black)
                                && (two_space & all_pieces).is_empty()))
                    {
                        legal_moves.push_low_priority(ts_move);
                    }
                }
            }
            if !(one_space.go_right() & enemy_pieces).is_empty() {
                if (os_row == 7 || os_row == 0)
                    && !self.would_be_in_check(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        0,
                    ))
                {
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        CAPTURE | PROMOTION,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_0,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_1,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_1 | SPECIAL_0,
                    ));
                } else if !self.would_be_in_check(Move::new(
                    pawn_col,
                    pawn_row,
                    os_col + 1,
                    os_row,
                    CAPTURE,
                )) {
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col + 1,
                        os_row,
                        CAPTURE,
                    ));
                }
            }
            if !(one_space.go_left() & enemy_pieces).is_empty() {
                if (os_row == 7 || os_row == 0)
                    && !self.would_be_in_check(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        0,
                    ))
                {
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        CAPTURE | PROMOTION,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_0,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_1,
                    ));
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        CAPTURE | PROMOTION | SPECIAL_1 | SPECIAL_0,
                    ));
                } else if !self.would_be_in_check(Move::new(
                    pawn_col,
                    pawn_row,
                    os_col - 1,
                    os_row,
                    CAPTURE,
                )) {
                    legal_moves.push_high_priority(Move::new(
                        pawn_col,
                        pawn_row,
                        os_col - 1,
                        os_row,
                        CAPTURE,
                    ));
                }
            }
            if !(one_space.go_right() & self.en_passant_square).is_empty() {
                let ep_move = Move::new(
                    pawn_col,
                    pawn_row,
                    os_col + 1,
                    os_row,
                    CAPTURE | SPECIAL_0,
                );
                if !self.would_be_in_check(ep_move) {
                    legal_moves.push_high_priority(ep_move);
                }
            }
            if !(one_space.go_left() & self.en_passant_square).is_empty() {
                let lp_move = Move::new(
                    pawn_col,
                    pawn_row,
                    os_col - 1,
                    os_row,
                    CAPTURE | SPECIAL_0,
                );
                if !self.would_be_in_check(lp_move) {
                    legal_moves.push_high_priority(lp_move);
                }
            }
        }
    }

    fn push_moves(
        &mut self,
        moves: BitBoard,
        from: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        for to in moves.get_set_bits_as_vector() {
            let (f_c, f_r) = from.get_lowest_bit_as_col_row().unwrap();
            let (t_c, t_r) = to.get_lowest_bit_as_col_row().unwrap();
            if self.board.into_iter().any(|b| !(b & to).is_empty()) {
                let m = Move::new(f_c, f_r, t_c, t_r, CAPTURE);
                if !self.would_be_in_check(m) {
                    legal_moves.push_medium_priority(m);
                }
            } else {
                let m = Move::new(f_c, f_r, t_c, t_r, 0);
                if !self.would_be_in_check(m) {
                    legal_moves.push_low_priority(m);
                }
            };
        }
    }

    fn knight_moves(
        &mut self,
        friendly_pieces: BitBoard,
        _enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::Knight);
        self.board[dummy]
            .get_set_bits_as_vector()
            .into_iter()
            .for_each(|k| {
                let moves = k.potential_knights_moves() & !friendly_pieces;
                self.push_moves(moves, k, legal_moves)
            })
    }

    fn bishop_moves(
        &mut self,
        friendly_pieces: BitBoard,
        enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::Bishop);
        self.board[dummy]
            .get_set_bits_as_vector()
            .into_iter()
            .for_each(|b| {
                let (col, row) = b.get_lowest_bit_as_col_row().unwrap();
                self.ray_cast_push(
                    b,
                    BitBoard::go_dl,
                    cmp::min(row, col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    b,
                    BitBoard::go_ur,
                    cmp::min(7 - row, 7 - col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    b,
                    BitBoard::go_ul,
                    cmp::min(7 - row, col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    b,
                    BitBoard::go_dr,
                    cmp::min(row, 7 - col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
            });
    }

    fn rook_moves(
        &mut self,
        friendly_pieces: BitBoard,
        enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::Rook);
        self.board[dummy]
            .get_set_bits_as_vector()
            .into_iter()
            .for_each(|r| {
                let (col, row) = r.get_lowest_bit_as_col_row().unwrap();
                self.ray_cast_push(
                    r,
                    BitBoard::go_down,
                    row,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    r,
                    BitBoard::go_up,
                    7 - row,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    r,
                    BitBoard::go_left,
                    col,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    r,
                    BitBoard::go_right,
                    7 - col,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
            });
    }

    fn queen_moves(
        &mut self,
        friendly_pieces: BitBoard,
        enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::Queen);
        self.board[dummy]
            .get_set_bits_as_vector()
            .into_iter()
            .for_each(|q| {
                let (col, row) = q.get_lowest_bit_as_col_row().unwrap();
                self.ray_cast_push(
                    q,
                    BitBoard::go_down,
                    row,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_up,
                    7 - row,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_left,
                    col,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_right,
                    7 - col,
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_dl,
                    cmp::min(row, col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_ur,
                    cmp::min(7 - row, 7 - col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_ul,
                    cmp::min(7 - row, col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
                self.ray_cast_push(
                    q,
                    BitBoard::go_dr,
                    cmp::min(row, 7 - col),
                    friendly_pieces,
                    enemy_pieces,
                    legal_moves,
                );
            });
    }

    fn king_moves(
        &mut self,
        friendly_pieces: BitBoard,
        _enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let dummy = Piece::from(self.player, Kind::King);
        let k = self.board[dummy];
        let moves = (k.go_down()
            | k.go_right()
            | k.go_left()
            | k.go_up()
            | k.go_dr()
            | k.go_ur()
            | k.go_ul()
            | k.go_dl())
            & !friendly_pieces;
        self.push_moves(moves, k, legal_moves);
        for c in self.check_castle() {
            legal_moves.push_high_priority(c)
        }
    }

    fn check_castle(&self) -> Vec<Move> {
        let squares_need_to_be_empty = match self.player {
            Player::White => [1 << 1 | 1 << 2 | 1 << 3, 1 << 5 | 1 << 6],
            Player::Black => [1 << 61 | 1 << 62, 1 << 57 | 1 << 58 | 1 << 59],
        };
        let castle_rights = match self.player {
            Player::White => [self.castle_right[1], self.castle_right[0]],
            Player::Black => [self.castle_right[2], self.castle_right[3]],
        };
        let king_moves_through_here = match self.player {
            Player::White => [[2, 3, 4], [6, 5, 4]],
            Player::Black => [[62, 61, 60], [58, 59, 60]],
        };
        let all_pieces = piece::LIST
            .iter()
            .fold(BitBoard::new(0), |pieces, &p| pieces | self.board[p]);
        squares_need_to_be_empty
            .iter()
            .zip(castle_rights.iter())
            .zip(king_moves_through_here.iter())
            .filter_map(|((&occupied, &right), &moves)| {
                let is_in_check = moves.iter().any(|&m| {
                    self.is_in_check(
                        BitBoard::new_coordinate_from_bit_index(m).unwrap(),
                    )
                });
                if (BitBoard::new(occupied) & all_pieces).is_empty()
                    && right
                    && !is_in_check
                {
                    let flags = if moves[0] & 7 == 2 {
                        SPECIAL_0 | SPECIAL_1
                    } else {
                        SPECIAL_1
                    };
                    Some(Move::new(
                        moves[2] & 7,
                        moves[2] >> 3,
                        moves[0] & 7,
                        moves[0] >> 3,
                        flags,
                    ))
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn get_king_loc(&self) -> BitBoard {
        self.board[Piece::from(self.player, Kind::King)]
    }

    pub fn is_in_check(&self, k: BitBoard) -> bool {
        let king_board = self.board[Piece::from(!self.player, Kind::King)];
        let rook_board = self.board[Piece::from(!self.player, Kind::Rook)];
        let pawn_board = self.board[Piece::from(!self.player, Kind::Pawn)];
        let bishop_board = self.board[Piece::from(!self.player, Kind::Bishop)];
        let knight_board = self.board[Piece::from(!self.player, Kind::Knight)];
        let queen_board = self.board[Piece::from(!self.player, Kind::Queen)];
        if !(pawn_board
            & (pawn_direction(self.player)(&k).go_left()
                | pawn_direction(self.player)(&k).go_right()))
        .is_empty()
        {
            return true;
        }
        if !(k.potential_knights_moves() & knight_board).is_empty() {
            return true;
        }
        let k_moves = k.go_up()
            | k.go_down()
            | k.go_left()
            | k.go_right()
            | k.go_dr()
            | k.go_dl()
            | k.go_ur()
            | k.go_ul();
        if !(k_moves & king_board).is_empty() {
            return true;
        }
        let (col, row) = k.get_lowest_bit_as_col_row().unwrap();
        let (enemy_pieces, friendly_pieces) = piece::LIST.iter().fold(
            (BitBoard::new(0), BitBoard::new(0)),
            |sum, &p| {
                if self.player == p.color {
                    (sum.0, sum.1 | self.board[p])
                } else {
                    (sum.0 | self.board[p], sum.1)
                }
            },
        );
        let mut b_moves = ray_cast(
            k,
            BitBoard::go_dl,
            cmp::min(row, col),
            friendly_pieces,
            enemy_pieces,
        );
        b_moves |= ray_cast(
            k,
            BitBoard::go_ur,
            cmp::min(7 - row, 7 - col),
            friendly_pieces,
            enemy_pieces,
        );
        b_moves |= ray_cast(
            k,
            BitBoard::go_ul,
            cmp::min(7 - row, col),
            friendly_pieces,
            enemy_pieces,
        );
        b_moves |= ray_cast(
            k,
            BitBoard::go_dr,
            cmp::min(row, 7 - col),
            friendly_pieces,
            enemy_pieces,
        );
        if !(b_moves & bishop_board).is_empty() {
            return true;
        }
        let mut r_moves =
            ray_cast(k, BitBoard::go_down, row, friendly_pieces, enemy_pieces);
        r_moves |= ray_cast(
            k,
            BitBoard::go_up,
            7 - row,
            friendly_pieces,
            enemy_pieces,
        );
        r_moves |=
            ray_cast(k, BitBoard::go_left, col, friendly_pieces, enemy_pieces);
        r_moves |= ray_cast(
            k,
            BitBoard::go_right,
            7 - col,
            friendly_pieces,
            enemy_pieces,
        );
        !(r_moves & rook_board).is_empty()
            || !((b_moves | r_moves) & queen_board).is_empty()
    }

    fn evaluate_freedom(&self) -> i16 {
        let (friendly_pieces, enemy_pieces) = piece::LIST
            .iter()
            .map(|&p| (p, self.board[p]))
            .fold((BitBoard::new(0), BitBoard::new(0)), |(f, e), (p, b)| {
                if p.color == self.player {
                    (f | b, e)
                } else {
                    (f, e | b)
                }
            });
        let mut f = 0;
        let knights = self.board[Piece::from(self.player, Kind::Knight)]
            .get_set_bits_as_vector();
        let bishops = self.board[Piece::from(self.player, Kind::Bishop)]
            .get_set_bits_as_vector();
        let rooks = self.board[Piece::from(self.player, Kind::Rook)]
            .get_set_bits_as_vector();
        let queens = self.board[Piece::from(self.player, Kind::Queen)]
            .get_set_bits_as_vector();
        f += knights
            .iter()
            .map(|p| p.potential_knights_moves().get_popcnt() as i16)
            .sum::<i16>();
        let diagonals = [
            BitBoard::go_dl,
            BitBoard::go_ul,
            BitBoard::go_dr,
            BitBoard::go_ur,
        ];
        let straights = [
            BitBoard::go_down,
            BitBoard::go_up,
            BitBoard::go_right,
            BitBoard::go_left,
        ];
        f += bishops
            .iter()
            .map(|p| {
                diagonals
                    .iter()
                    .map(|d| {
                        ray_cast_count(*p, *d, friendly_pieces, enemy_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        f += rooks
            .iter()
            .map(|p| {
                straights
                    .iter()
                    .map(|d| {
                        ray_cast_count(*p, *d, friendly_pieces, enemy_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        f += queens
            .iter()
            .map(|p| {
                straights
                    .iter()
                    .chain(diagonals.iter())
                    .map(|d| {
                        ray_cast_count(*p, *d, friendly_pieces, enemy_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        let e_knights = self.board[Piece::from(!self.player, Kind::Knight)]
            .get_set_bits_as_vector();
        let e_bishops = self.board[Piece::from(!self.player, Kind::Bishop)]
            .get_set_bits_as_vector();
        let e_rooks = self.board[Piece::from(!self.player, Kind::Rook)]
            .get_set_bits_as_vector();
        let e_queens = self.board[Piece::from(!self.player, Kind::Queen)]
            .get_set_bits_as_vector();
        f -= e_knights
            .iter()
            .map(|p| p.potential_knights_moves().get_popcnt() as i16)
            .sum::<i16>();
        let diagonals = [
            BitBoard::go_dl,
            BitBoard::go_ul,
            BitBoard::go_dr,
            BitBoard::go_ur,
        ];
        let straights = [
            BitBoard::go_down,
            BitBoard::go_up,
            BitBoard::go_right,
            BitBoard::go_left,
        ];
        f -= e_bishops
            .iter()
            .map(|p| {
                diagonals
                    .iter()
                    .map(|d| {
                        ray_cast_count(*p, *d, enemy_pieces, friendly_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        f -= e_rooks
            .iter()
            .map(|p| {
                straights
                    .iter()
                    .map(|d| {
                        ray_cast_count(*p, *d, enemy_pieces, friendly_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        f -= e_queens
            .iter()
            .map(|p| {
                straights
                    .iter()
                    .chain(diagonals.iter())
                    .map(|d| {
                        ray_cast_count(*p, *d, enemy_pieces, friendly_pieces)
                    })
                    .sum::<i16>()
            })
            .sum::<i16>();
        f
    }

    pub fn evaluate(&mut self) -> i16 {
        let material_difference = [
            Kind::Pawn,
            Kind::Knight,
            Kind::Bishop,
            Kind::Rook,
            Kind::Queen,
        ]
        .iter()
        .map(|&k| {
            self.board[Piece::from(self.player, k)].get_popcnt() as i16
                - self.board[Piece::from(!self.player, k)].get_popcnt() as i16
        })
        .zip([100, 280, 300, 500, 800].iter())
        .fold(0, |sum, (c, w)| sum + c * w);
        material_difference + 10 * self.evaluate_freedom()
            - 50 * self.blocked_pawns()
            - 50 * self.isolated_pawns()
            - 50 * self.doubled_pawns()
            + self.king_safety()
    }

    fn king_safety(&self) -> i16 {
        if !self.board[Piece::from(!self.player, Kind::Queen)].is_empty() {
            let my_king = self.board[Piece::from(self.player, Kind::King)];
            let enemy_king = self.board[Piece::from(self.player, Kind::King)];
            let mut safety = 0;
            let safe_file = match self.player {
                Player::White => BitBoard::new(FIRST_RANK),
                Player::Black => BitBoard::new(EIGTH_RANK),
            };
            if (my_king & safe_file).is_empty() {
                safety -= 50;
            }
            if (enemy_king & safe_file).is_empty() {
                safety += 50;
            }
            let castled_squares = [
                safe_file & BitBoard::new(C_FILE),
                safe_file & BitBoard::new(G_FILE),
            ];
            if (my_king & castled_squares[0]).is_empty()
                || (my_king & castled_squares[1]).is_empty()
            {
                safety -= 100;
            }
            if (enemy_king & castled_squares[0]).is_empty()
                || (enemy_king & castled_squares[1]).is_empty()
            {
                safety += 100;
            }
            let my_vertical_pawn = pawn_direction(self.player)(&my_king);
            let enemy_vertical_pawn = pawn_direction(self.player)(&enemy_king);
            let my_pawn_shield = my_vertical_pawn.go_left()
                | my_vertical_pawn
                | my_vertical_pawn.go_right();
            let enemy_pawn_shield = enemy_vertical_pawn.go_left()
                | enemy_vertical_pawn
                | enemy_vertical_pawn.go_right();
            safety += ((my_pawn_shield
                & self.board[Piece::from(self.player, Kind::Pawn)])
            .get_popcnt()
                * 100) as i16;
            safety -= ((enemy_pawn_shield
                & self.board[Piece::from(self.player, Kind::Pawn)])
            .get_popcnt()
                * 100) as i16;
            safety
        } else {
            0
        }
    }

    fn doubled_pawns(&self) -> i16 {
        let mut doubled = 0;
        for i in 0..8 {
            let mut my_pawns = (self.board
                [Piece::from(self.player, Kind::Pawn)]
                & BitBoard::new(A_FILE << i))
            .get_popcnt() as i16;
            let mut his_pawns = (self.board
                [Piece::from(!self.player, Kind::Pawn)]
                & BitBoard::new(A_FILE << i))
            .get_popcnt() as i16;
            if my_pawns > 0 {
                my_pawns -= 1;
            }
            if his_pawns > 0 {
                his_pawns -= 1;
            }
            doubled += my_pawns - his_pawns;
        }
        doubled
    }

    fn isolated_pawns(&self) -> i16 {
        let my_pawns = self.board[Piece::from(self.player, Kind::Pawn)];
        let his_pawns = self.board[Piece::from(!self.player, Kind::Pawn)];
        let mut isolated = 0;
        for i in 0..8 {
            let mut adjacent_files_u64 = 0;
            if i < 7 {
                adjacent_files_u64 |= B_FILE << i;
            }
            if i > 0 {
                adjacent_files_u64 |= A_FILE << (i - 1);
            }
            let adjacent_files = BitBoard::new(adjacent_files_u64);
            let my_isolated_pawns = if (my_pawns & adjacent_files).is_empty() {
                (my_pawns & BitBoard::new(A_FILE << i)).get_popcnt()
            } else {
                0
            };
            let his_isolated_pawns = if (his_pawns & adjacent_files).is_empty()
            {
                (his_pawns & BitBoard::new(A_FILE << i)).get_popcnt()
            } else {
                0
            };
            isolated += my_isolated_pawns as i16 - his_isolated_pawns as i16;
        }
        isolated
    }

    fn blocked_pawns(&self) -> i16 {
        let mut blocked = 0;
        for i in 0..8 {
            let my_pawns = self.board[Piece::from(self.player, Kind::Pawn)]
                & BitBoard::new(A_FILE << i);
            let his_pawns = self.board[Piece::from(!self.player, Kind::Pawn)]
                & BitBoard::new(A_FILE << i);
            let my_blocked_pawns = my_pawns
                .get_set_bits_as_vector()
                .iter()
                .filter(|pawn| {
                    piece::LIST.iter().any(|&piece| {
                        !(pawn_direction(self.player)(pawn) & self.board[piece])
                            .is_empty()
                    })
                })
                .count();
            let his_blocked_pawns = his_pawns
                .get_set_bits_as_vector()
                .iter()
                .filter(|pawn| {
                    piece::LIST.iter().any(|&piece| {
                        !(pawn_direction(!self.player)(pawn)
                            & self.board[piece])
                            .is_empty()
                    })
                })
                .count();
            blocked += my_blocked_pawns as i16 - his_blocked_pawns as i16;
        }
        blocked
    }

    pub fn get_zobrist_key(&self) -> u64 {
        self.zobrist.get_key()
    }

    pub fn perft_node(&mut self, depth: usize) -> u64 {
        if depth == 0 {
            1
        } else {
            self.get_legal_moves()
                .map(|m| {
                    self.make_move(m);
                    let n = self.perft_node(depth - 1);
                    self.unmake_move(m);
                    n
                })
                .sum()
        }
    }

    #[inline]
    fn ray_cast_push(
        &mut self,
        from: BitBoard,
        direction: fn(&BitBoard) -> BitBoard,
        n: usize,
        friendly_pieces: BitBoard,
        enemy_pieces: BitBoard,
        legal_moves: &mut LegalMoves,
    ) {
        let mut to = from;
        let (from_col, from_row) = from.get_lowest_bit_as_col_row().unwrap();
        for _i in 0..n {
            to = direction(&to);
            let (to_col, to_row) = to.get_lowest_bit_as_col_row().unwrap();
            if !(to & friendly_pieces).is_empty() {
                break;
            } else if !(to & enemy_pieces).is_empty() {
                let m = Move::new(from_col, from_row, to_col, to_row, CAPTURE);
                if !self.would_be_in_check(m) {
                    legal_moves.push_medium_priority(m);
                }
                break;
            } else {
                let m = Move::new(from_col, from_row, to_col, to_row, 0);
                if !self.would_be_in_check(m) {
                    legal_moves.push_medium_priority(m);
                }
            }
        }
    }
}

#[inline]
fn ray_cast_count(
    mut loc: BitBoard,
    direction: fn(&BitBoard) -> BitBoard,
    friendly_pieces: BitBoard,
    enemy_pieces: BitBoard,
) -> i16 {
    let mut c = 0;
    while !loc.is_empty() {
        loc = direction(&loc);
        if !(loc & friendly_pieces).is_empty() {
            return c;
        } else if !(loc & enemy_pieces).is_empty() {
            return c + 1;
        } else {
            c += 1;
        }
    }
    c
}

#[inline]
fn ray_cast(
    mut loc: BitBoard,
    direction: fn(&BitBoard) -> BitBoard,
    n: usize,
    friendly_pieces: BitBoard,
    enemy_pieces: BitBoard,
) -> BitBoard {
    let mut moves = BitBoard::new(0);
    for _i in 0..n {
        loc = direction(&loc);
        if !(loc & friendly_pieces).is_empty() {
            break;
        } else if !(loc & enemy_pieces).is_empty() {
            moves |= loc;
            break;
        } else {
            moves |= loc;
        }
    }
    moves
}

#[inline]
fn pawn_direction(player: Player) -> fn(&BitBoard) -> BitBoard {
    match player {
        Player::Black => BitBoard::go_down,
        Player::White => BitBoard::go_up,
    }
}
