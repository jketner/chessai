use crate::bitboard::BitBoard;
use crate::piece::{self, kind::Kind, player::Player, Piece};
use std::ops::{Index, IndexMut};

#[derive(Debug)]
pub struct Boards {
    /*white_pawn: BitBoard,
    white_knight: BitBoard,
    white_bishop: BitBoard,
    white_rook: BitBoard,
    white_queen: BitBoard,
    white_king: BitBoard,
    black_pawn: BitBoard,
    black_knight: BitBoard,
    black_bishop: BitBoard,
    black_rook: BitBoard,
    black_queen: BitBoard,
    black_king: BitBoard,*/
    boards: [BitBoard; 12],
}

impl Copy for Boards {}

impl Clone for Boards {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

impl Boards {
    pub fn new() -> Self {
        Boards {
            /*white_pawn: BitBoard::new(0),
            white_knight: BitBoard::new(0),
            white_bishop: BitBoard::new(0),
            white_rook: BitBoard::new(0),
            white_queen: BitBoard::new(0),
            white_king: BitBoard::new(0),
            black_pawn: BitBoard::new(0),
            black_knight: BitBoard::new(0),
            black_bishop: BitBoard::new(0),
            black_rook: BitBoard::new(0),
            black_queen: BitBoard::new(0),
            black_king: BitBoard::new(0),*/
            boards: [BitBoard::new(0); 12],
        }
    }
}

impl Index<Piece> for Boards {
    type Output = BitBoard;

    fn index(&self, piece: Piece) -> &Self::Output {
        match piece {
            Piece {
                color: Player::White,
                kind: Kind::Pawn,
            } => &self.boards[0],
            Piece {
                color: Player::White,
                kind: Kind::Knight,
            } => &self.boards[1],
            Piece {
                color: Player::White,
                kind: Kind::Bishop,
            } => &self.boards[2],
            Piece {
                color: Player::White,
                kind: Kind::Rook,
            } => &self.boards[3],
            Piece {
                color: Player::White,
                kind: Kind::Queen,
            } => &self.boards[4],
            Piece {
                color: Player::White,
                kind: Kind::King,
            } => &self.boards[5],
            Piece {
                color: Player::Black,
                kind: Kind::Pawn,
            } => &self.boards[6],
            Piece {
                color: Player::Black,
                kind: Kind::Knight,
            } => &self.boards[7],
            Piece {
                color: Player::Black,
                kind: Kind::Bishop,
            } => &self.boards[8],
            Piece {
                color: Player::Black,
                kind: Kind::Rook,
            } => &self.boards[9],
            Piece {
                color: Player::Black,
                kind: Kind::Queen,
            } => &self.boards[10],
            Piece {
                color: Player::Black,
                kind: Kind::King,
            } => &self.boards[11],
        }
    }
}

impl IndexMut<Piece> for Boards {
    fn index_mut(&mut self, piece: Piece) -> &mut Self::Output {
        match piece {
            Piece {
                color: Player::White,
                kind: Kind::Pawn,
            } => &mut self.boards[0],
            Piece {
                color: Player::White,
                kind: Kind::Knight,
            } => &mut self.boards[1],
            Piece {
                color: Player::White,
                kind: Kind::Bishop,
            } => &mut self.boards[2],
            Piece {
                color: Player::White,
                kind: Kind::Rook,
            } => &mut self.boards[3],
            Piece {
                color: Player::White,
                kind: Kind::Queen,
            } => &mut self.boards[4],
            Piece {
                color: Player::White,
                kind: Kind::King,
            } => &mut self.boards[5],
            Piece {
                color: Player::Black,
                kind: Kind::Pawn,
            } => &mut self.boards[6],
            Piece {
                color: Player::Black,
                kind: Kind::Knight,
            } => &mut self.boards[7],
            Piece {
                color: Player::Black,
                kind: Kind::Bishop,
            } => &mut self.boards[8],
            Piece {
                color: Player::Black,
                kind: Kind::Rook,
            } => &mut self.boards[9],
            Piece {
                color: Player::Black,
                kind: Kind::Queen,
            } => &mut self.boards[10],
            Piece {
                color: Player::Black,
                kind: Kind::King,
            } => &mut self.boards[11],
        }
    }
}

impl IntoIterator for Boards {
    type Item = BitBoard;
    type IntoIter = BoardsIntoIterator;

    fn into_iter(self) -> Self::IntoIter {
        BoardsIntoIterator {
            boards: self,
            index: 0,
        }
    }
}

pub struct BoardsIntoIterator {
    boards: Boards,
    index: usize,
}

impl Iterator for BoardsIntoIterator {
    type Item = BitBoard;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < 12 {
            self.index += 1;
            Some(self.boards[piece::LIST[self.index - 1]])
        } else {
            None
        }
    }
}
