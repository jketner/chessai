use crate::chess_move::Move;

pub struct LegalMoves {
    high_priority_moves: Vec<Move>,
    medium_priority_moves: Vec<Move>,
    low_priority_moves: Vec<Move>,
}

impl LegalMoves {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            high_priority_moves: Vec::with_capacity(10),
            medium_priority_moves: Vec::with_capacity(10),
            low_priority_moves: Vec::with_capacity(10),
        }
    }

    #[inline]
    pub fn pop(&mut self) -> Option<Move> {
        if self.high_priority_moves.is_empty() {
            if self.medium_priority_moves.is_empty() {
                if self.low_priority_moves.is_empty() {
                    None
                } else {
                    self.low_priority_moves.pop()
                }
            } else {
                self.medium_priority_moves.pop()
            }
        } else {
            self.high_priority_moves.pop()
        }
    }

    #[inline]
    pub fn swap_to_front(&mut self, m: Move) {
        for i in 0..self.high_priority_moves.len() {
            if self.high_priority_moves[i] == m {
                self.high_priority_moves.remove(i);
                self.high_priority_moves.push(m);
                return;
            }
        }
        for i in 0..self.medium_priority_moves.len() {
            if self.medium_priority_moves[i] == m {
                self.medium_priority_moves.remove(i);
                self.high_priority_moves.push(m);
                return;
            }
        }
        for i in 0..self.low_priority_moves.len() {
            if self.low_priority_moves[i] == m {
                self.low_priority_moves.remove(i);
                self.high_priority_moves.push(m);
                return;
            }
        }
    }

    #[inline]
    pub fn get_first(&self) -> Move {
        let n = self.high_priority_moves.len();
        let m = self.medium_priority_moves.len();
        let o = self.low_priority_moves.len();
        if n > 0 {
            self.high_priority_moves[n - 1]
        } else if m > 0 {
            self.medium_priority_moves[m - 1]
        } else if o > 0 {
            self.low_priority_moves[o - 1]
        } else {
            panic!()
        }
    }

    #[inline]
    pub fn push_high_priority(&mut self, m: Move) {
        self.high_priority_moves.push(m);
    }

    #[inline]
    pub fn push_medium_priority(&mut self, m: Move) {
        self.medium_priority_moves.push(m);
    }

    #[inline]
    pub fn push_low_priority(&mut self, m: Move) {
        self.low_priority_moves.push(m);
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.high_priority_moves.len()
            + self.medium_priority_moves.len()
            + self.low_priority_moves.len()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.high_priority_moves.is_empty()
            && self.medium_priority_moves.is_empty()
            && self.low_priority_moves.is_empty()
    }
}

impl Iterator for LegalMoves {
    type Item = Move;

    #[inline]
    fn next(&mut self) -> Option<Move> {
        self.pop()
    }
}
