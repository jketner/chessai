use std::ops::Not;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Player {
    Black,
    White,
}

impl Not for Player {
    type Output = Player;

    fn not(self) -> Player {
        match self {
            Player::Black => Player::White,
            Player::White => Player::Black,
        }
    }
}
