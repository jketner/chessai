pub mod kind;
pub mod player;
use kind::Kind;
use player::Player;
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Piece {
    pub color: Player,
    pub kind: Kind,
}

pub const LIST: [Piece; 12] = [
    Piece {
        color: Player::Black,
        kind: Kind::Pawn,
    },
    Piece {
        color: Player::White,
        kind: Kind::Pawn,
    },
    Piece {
        color: Player::Black,
        kind: Kind::Knight,
    },
    Piece {
        color: Player::White,
        kind: Kind::Knight,
    },
    Piece {
        color: Player::Black,
        kind: Kind::Bishop,
    },
    Piece {
        color: Player::White,
        kind: Kind::Bishop,
    },
    Piece {
        color: Player::Black,
        kind: Kind::Rook,
    },
    Piece {
        color: Player::White,
        kind: Kind::Rook,
    },
    Piece {
        color: Player::Black,
        kind: Kind::Queen,
    },
    Piece {
        color: Player::White,
        kind: Kind::Queen,
    },
    Piece {
        color: Player::Black,
        kind: Kind::King,
    },
    Piece {
        color: Player::White,
        kind: Kind::King,
    },
];

impl fmt::Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self.color {
                Player::Black => match self.kind {
                    Kind::King => "♔ |",
                    Kind::Queen => "♕ |",
                    Kind::Rook => "♖ |",
                    Kind::Bishop => "♗ |",
                    Kind::Knight => "♘ |",
                    Kind::Pawn => "♙ |",
                },
                Player::White => match self.kind {
                    Kind::King => "♚ |",
                    Kind::Queen => "♛ |",
                    Kind::Rook => "♜ |",
                    Kind::Bishop => "♝ |",
                    Kind::Knight => "♞ |",
                    Kind::Pawn => "♟ |",
                },
            }
        )
    }
}

impl Piece {
    pub fn from(color: Player, kind: Kind) -> Self {
        Piece { color, kind }
    }

    pub fn get_color(&self) -> Player {
        self.color
    }

    pub fn get_kind(&self) -> Kind {
        self.kind
    }
}
