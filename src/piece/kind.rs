#[derive(PartialEq, Debug, Clone, Copy, Eq, Hash)]
pub enum Kind {
    King,
    Queen,
    Rook,
    Bishop, Knight,
    Pawn
}
