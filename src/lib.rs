#[macro_use]
extern crate impl_ops;
use std::process::{Command, Stdio};
use std::str;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use std::time::{Duration, Instant};
extern crate regex;
use std::collections::HashMap;
use std::io::{self, Write};

use regex::Regex;

pub mod bitboard;
pub mod board;
pub mod chess_move;
mod engine_thread;
pub mod piece;

use board::Board;
use engine_thread::EngineThread;

pub fn stockfish_perft(fen_string: &str, perft_index: usize) {
    let mut v = String::new();
    for i in (1..=perft_index).rev() {
        let (stockfish_map, stock_fen) =
            priv_stockfish_perft(fen_string, &v, i);
        println!("{}", v);
        let (my_map, _sum) = divide(&stock_fen, i).unwrap();
        let m_unique_key =
            my_map.keys().find(|&k| !stockfish_map.contains_key(k));
        let s_unique_key =
            stockfish_map.keys().find(|&k| !my_map.contains_key(k));
        match (s_unique_key, m_unique_key) {
            (Some(key), _) => {
                let mut board = Board::new();
                board.initialize(&stock_fen).unwrap();
                board.print_board();
                println!("{}: move not in my version", key);
                return;
            }
            (_, Some(key)) => {
                let mut board = Board::new();
                board.initialize(&stock_fen).unwrap();
                board.print_board();
                println!("{}: extra move in my version", key);
                return;
            }
            (None, None) => {
                match stockfish_map.keys().find(|k| {
                    my_map.get(*k).unwrap() != stockfish_map.get(*k).unwrap()
                }) {
                    Some(key) => {
                        v.push_str(key);
                        v.push(' ');
                    }
                    None => {
                        println!("all the same!");
                        return;
                    }
                }
            }
        };
    }
}

fn priv_stockfish_perft(
    fen_string: &str,
    moves: &str,
    perft_index: usize,
) -> (HashMap<String, u64>, String) {
    let mut stockfish = Command::new("/usr/games/stockfish")
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("stockfish failed to start");
    let mut position_string = "position ".to_string();
    if fen_string != "startpos" {
        position_string.push_str("fen ");
    }
    position_string.push_str(fen_string);
    if moves.len() > 0 {
        position_string.push_str(" moves ");
        position_string.push_str(moves);
    }
    position_string.push('\n');
    let mut perft_string = "perft ".to_string();
    perft_string.push_str(&perft_index.to_string());
    {
        let stdin = stockfish.stdin.as_mut().expect("failed to get stdin");
        stdin
            .write_all(position_string.as_bytes())
            .expect("failed to write stdin");
        stdin.write_all(b"d\n").expect("failed to write stdin");
        stdin
            .write_all(perft_string.as_bytes())
            .expect("failed to write stdin");
    }
    let output = stockfish
        .wait_with_output()
        .expect("failed to wait on stockfish")
        .stdout;
    let starts_with_move = Regex::new(r"^[a-z]\d[a-z]\d").unwrap();
    let mut h = HashMap::new();
    let mut fen_out = String::new();
    for line in String::from_utf8(output).unwrap().lines() {
        if starts_with_move.is_match(line) {
            let mut split = line.split(": ");
            h.insert(
                split.next().unwrap().to_string(),
                u64::from_str_radix(split.next().unwrap(), 10).unwrap(),
            );
        } else if line.starts_with("Fen: ") {
            fen_out.push_str(&line[5..]);
        }
    }
    (h, fen_out)
}

pub fn divide(
    position: &str,
    depth: usize,
) -> Result<(HashMap<String, u64>, u64), String> {
    let instant = Instant::now();
    if depth == 0 {
        return Err("cannot divide depth 0".to_string());
    }
    let mut board = Board::new();
    board.initialize(position)?;
    board.print_board();
    let mut h = HashMap::new();
    let mut sum = 0;
    for m in board.get_legal_moves() {
        board.make_move(m);
        let n = board.perft_node(depth - 1);
        board.unmake_move(m);
        println!("{}: {}", m, n);
        h.insert(format!("{}", m), n);
        sum += n;
    }
    println!("{} seconds", instant.elapsed().as_secs());
    println!("{} nodes searched", sum);
    Ok((h, sum))
}

const COMMANDS: [&str; 11] = [
    "uci",
    "debug",
    "isready",
    "setoption",
    "register",
    "ucinewgame",
    "position",
    "go",
    "stop",
    "ponderhit",
    "quit",
];
const ENGINE_NAME: &str = "alpha_beta_rust";

pub fn universal_chess_interface() {
    let (tx, rx) = mpsc::channel();
    EngineThread::spawn(rx);
    loop {
        let mut arc_input = Arc::new(String::new());
        let mut input = Arc::get_mut(&mut arc_input).unwrap();
        match io::stdin().read_line(&mut input) {
            Err(_) => println!("info string failed to read input"),
            Ok(_) => (),
        }
        match COMMANDS.iter().flat_map(|c| input.find(c)).min() {
            None => input.clear(),
            Some(i) => input.replace_range(0..i, ""),
        }
        if input == "uci\n" {
            println!("id name {}", ENGINE_NAME);
            println!("id author Jonathan Ketner");
            println!("uciok");
        } else if input.starts_with("debug") {
        } else if input.starts_with("isready") {
            println!("readyok");
        } else if input.starts_with("setoption") {
        } else if input.starts_with("register") {
        } else if input.starts_with("ucinewgame") {
            tx.send(arc_input).unwrap();
        } else if input.starts_with("position") {
            if &input[9..17] == "startpos" {
                input.replace_range(
                    9..17,
                    "fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
                );
            }
            tx.send(arc_input).unwrap();
        } else if input.starts_with("go") {
            tx.send(arc_input).unwrap();
        } else if input.starts_with("stop") {
            tx.send(arc_input).unwrap();
        } else if input.starts_with("ponderhit") {
        } else if input.starts_with("quit") {
            return;
        }
        thread::sleep(Duration::from_millis(50));
    }
}
